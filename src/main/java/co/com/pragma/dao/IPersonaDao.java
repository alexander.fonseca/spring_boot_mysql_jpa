package co.com.pragma.dao;


import co.com.pragma.domain.Persona;
import org.springframework.data.repository.CrudRepository;


public interface IPersonaDao extends CrudRepository<Persona,Long> {}
