package co.com.pragma.ejerciciobasedatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerciciobasedatosApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjerciciobasedatosApplication.class, args);
	}

}
