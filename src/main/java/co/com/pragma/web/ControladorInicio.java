package co.com.pragma.web;

import co.com.pragma.dao.IPersonaDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class ControladorInicio {

    @Autowired
    private IPersonaDao iPersonaDao;

    @GetMapping("/")
    public String inicio(Model model){
        var personas = iPersonaDao.findAll();
        model.addAttribute("personas",personas);
        return "index";
    }

}
